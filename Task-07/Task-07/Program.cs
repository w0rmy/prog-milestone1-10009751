﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_07
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 12;
            var i = 0;

            Console.WriteLine("Please enter a number to show its times table");
            var number = int.Parse(Console.ReadLine());
            Console.Clear();

            for (i = 0; i < counter; i++)
            {
                var times = i + 1;
                Console.WriteLine($"{number} x {times} = " + (number * times));
            }
            
        }
    }
}
