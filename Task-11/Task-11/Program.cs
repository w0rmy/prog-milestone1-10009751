﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 0;

            Console.WriteLine("Enter a year to check if its a leap year");
            year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
            {
                Console.WriteLine("{0} is a Leap Year", year);
            }
            else if (year % 100 == 0)
            {
                Console.WriteLine($"{year} is not a Leap Year");
            }
            else if (year % 4 == 0)
            {
                Console.WriteLine(year + " is a Leap Year");
            }
            else
            {
                Console.WriteLine($"{year} is not a Leap Year");
            }
        }
    }
}
