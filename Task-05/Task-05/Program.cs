﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a 24hr time to show am/pm equiv (In the format of a number 1-24)");
            var tfhr = int.Parse(Console.ReadLine());
            
            if (tfhr > 12)
            {
                Console.WriteLine((tfhr - 12) + "pm");
            }
            else
            {
                Console.WriteLine($"{tfhr}am");
            }
        }
    }
}
