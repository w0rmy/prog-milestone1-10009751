﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 0;
            var number = 0;

            Console.WriteLine("Please enter 5 numbers to add together");
           
            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"Input number #{a}");
                number = number + int.Parse(Console.ReadLine());
            }
            Console.WriteLine($"Total: {number}");
        }
    }
}
