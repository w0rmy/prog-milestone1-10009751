﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number to check if its odd or even");
            var number = int.Parse(Console.ReadLine());

            if ((number % 2) == 1)
            {
                Console.WriteLine($"{number} is odd");
            }
            else
            {
                Console.WriteLine($"{number} is even");
            }
        }
    }
}
