﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.WriteLine("What is your age?");
            var age = Console.ReadLine();
            Console.WriteLine($"Your name is: {name}");
            Console.WriteLine("And your age is: " + age );
            Console.WriteLine("It was nice to meet you {0}", name);
        }
    }
}
