﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please select an option:");
            Console.WriteLine("a: Convert Celsius to Fahrenheit");
            Console.WriteLine("b: Convert Farenheit to Celsius");

            var answer = Console.ReadLine();

            if (answer == "a")
            {
                Console.WriteLine("What temperature in Celcius would you like converted to Fahrenheit?");
                var c2fq = double.Parse(Console.ReadLine());
                var c2fa = c2fq * 9 / 5 + 32;
                Console.WriteLine($"{c2fq} celcius is equal to {c2fa} fahrenheit");
            }
            if (answer == "b")
            {
                Console.WriteLine("What temperature in Fahrenheit would you like converted to Celcius?");
                var f2cq = double.Parse(Console.ReadLine());
                var f2ca = (f2cq - 32) * 5 /9;
                Console.WriteLine($"{f2cq} Farenheit is equal to {f2ca} Celcius");
            }
            else
            {
                Console.WriteLine("You have entered an incorrect value");
            }
            
        }
    }
}
