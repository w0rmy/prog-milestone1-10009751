﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            double credits = 15;
            double hpc = 10;
            double ch = 5;
            double weeks = 12;

            double result = (((hpc * credits) - (ch * weeks)) / weeks);

            Console.WriteLine($"{result}");
        }
    }
}
