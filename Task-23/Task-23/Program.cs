﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {

            var weekdays = new Dictionary<string, bool>();
            weekdays.Add("Monday", false);
            weekdays.Add("Tuesday", false);
            weekdays.Add("Wednesday", false);
            weekdays.Add("Thursday", false);
            weekdays.Add("Friday", false);
            weekdays.Add("Saturday", true);
            weekdays.Add("Sunday", true);

            
            foreach (KeyValuePair<string, bool> weekend in weekdays)
            {
                if (weekend.Value.Equals(true))
                {
                    Console.WriteLine(weekend.Key.ToString() + " is a weekend.");
                }

                else
                {
                    Console.WriteLine(weekend.Key.ToString() + " is a weekday.");
                }
            }
        }
    }
}
