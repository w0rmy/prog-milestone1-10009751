﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new string[7] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            var i = 0;

            foreach (var x in days)
            {
                i++;
                Console.WriteLine($"{x} is day {i} of the week");
            }
        }
    }
}
