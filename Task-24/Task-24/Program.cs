﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var menuchoice = "m";
            while (menuchoice == "m")
            {

                Console.Clear();
                Console.WriteLine("Please select an option:");
                Console.WriteLine("a. Option A");
                Console.WriteLine("b. Option B");
                Console.WriteLine("c. Option C");


                menuchoice = Console.ReadLine();
                menuchoice = menuchoice.ToLower();

                switch (menuchoice)
                {
                    case "a":
                        Console.Clear();
                        Console.WriteLine("Thing A has been done");
                        Console.WriteLine("Hit m for menu");
                        menuchoice = Console.ReadLine();
                        menuchoice = menuchoice.ToLower();
                        break;
                    case "b":
                        Console.Clear();
                        Console.WriteLine("Thing B has been done");
                        Console.WriteLine("Hit m for menu");
                        menuchoice = Console.ReadLine();
                        menuchoice = menuchoice.ToLower();
                        break;
                    case "c":
                        Console.Clear();
                        Console.WriteLine("Thing C has been done");
                        Console.WriteLine("Hit m for menu");
                        menuchoice = Console.ReadLine();
                        menuchoice = menuchoice.ToLower();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Sorry, invalid selection");
                        Console.WriteLine("Hit m for menu");
                        menuchoice = Console.ReadLine();
                        menuchoice = menuchoice.ToLower();
                        break;
                }
            }
        }
    }
}
