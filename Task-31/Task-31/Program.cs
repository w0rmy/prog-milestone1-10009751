﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to check if its modulus by 3 and 4");

            var number = int.Parse(Console.ReadLine());
            var a = number % 3;
            var b = number % 4;

            
            if (a == 0)
            {
                if (b == 0)
                {
                    Console.WriteLine($"Your number {number} is modulus by 3 and 4");
                }
                else
                {
                    Console.WriteLine($"Nope {number} isnt.");
                }
            }
            else
            {
                Console.WriteLine($"Nope {number} isnt.");
            }
        }
    }
}
