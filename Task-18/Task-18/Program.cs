﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new List<Tuple<String, string, int>>();
            user.Add(Tuple.Create("Jeremy", "May", 4));
            user.Add(Tuple.Create("Carol", "April", 29));
            user.Add(Tuple.Create("John", "Jan", 12));
            Console.WriteLine(string.Join(", ", user));
            Console.WriteLine($"My name is {user[0].Item1}, I was born on the {user[0].Item3}th of {user[0].Item2}");
            Console.WriteLine(user[1].Item1 + " is my mothers name.");
            // Learnt this, like it better
            user.ForEach(Console.WriteLine);
        }
    }
}
