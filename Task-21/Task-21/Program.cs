﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create dictionary
            var months = new Dictionary<string, int>();
            
            months.Add("January", 31);
            months.Add("February", 28);
            months.Add("March", 31);
            months.Add("April", 30);
            months.Add("May", 31);
            months.Add("June", 30);
            months.Add("July", 31);
            months.Add("August", 31);
            months.Add("September", 30);
            months.Add("October", 31);
            months.Add("November", 30);
            months.Add("December", 31);
            
            //Iterate dictionary
            foreach (KeyValuePair<string, int> month in months)
            {
                //Match on value
                if (month.Value.Equals(31))
                {
                    //Display results converting them to string
                    Console.WriteLine(month.Key.ToString() + " has " + month.Value.ToString() + " days.");
                }

                else
                {
                    //Do nothing
                }
            }
        }
    }
}
