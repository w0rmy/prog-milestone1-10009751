﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int> {};

            Console.WriteLine("Please enter 5 numbers");
            Console.WriteLine("Number 1");
            numbers.Add(int.Parse(Console.ReadLine()));
            Console.WriteLine("Number 2");
            numbers.Add(int.Parse(Console.ReadLine()));
            Console.WriteLine("Number 3");
            numbers.Add(int.Parse(Console.ReadLine()));
            Console.WriteLine("Number 4");
            numbers.Add(int.Parse(Console.ReadLine()));
            Console.WriteLine("Number 5");
            numbers.Add(int.Parse(Console.ReadLine()));

            int sum = numbers[0] + numbers[1] + numbers[2] + numbers[3] + numbers[4];

            string numberstring = string.Join(",", numbers);

            Console.WriteLine($"The sum of your numbers, {numberstring} is: {sum} ");
        }
    }
}
