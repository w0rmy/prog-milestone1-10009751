﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 12;
            var i = 0;

            Console.WriteLine("Please enter a number to show its division table");
            var number = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine($"Division table for {number}");
            for (i = 0; i < counter; i++)
            {
                var div = i + 1;
                Console.WriteLine($"{number} / {div} = " + (number / div));
            }
        }
    }
}
