﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 3 words");
            var input = Console.ReadLine();
            var output = input.Split(' ');
            foreach (var x in output)
            {
                Console.WriteLine(x);
            }
        }
    }
}
